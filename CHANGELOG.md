This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Accounting Service

## [v2.1.0-SNAPSHOT]

- Added request filter to allow the service to properly run with any Smartgears version 

## [v2.0.0] 

- Switched analytics persistence to PostgreSQL [#21182]
- Switched JSON management to gcube-jackson [#19115]
- Fixed distro files and pom according to new release procedure


## [v1.3.0] [r4.11.0] - 2018-04-12

- Added Regex rule support with reload functionalities [#11232]


## [v1.2.0] [r4.10.0] - 2018-02-15

- Reorganize URLs to be REST compliant [#10810]


## [v1.1.0] [r4.7.0] - 2017-10-09

- Complete Refactor of Service [#9032]
- Record List is unmarshalled as JSON using jackson [#9033] [#9036]
- Always posting a List of Record even when we have to post a single one [#9034]


## [v1.0.0] [r4.5.0] - 2017-06-07

- First Release

